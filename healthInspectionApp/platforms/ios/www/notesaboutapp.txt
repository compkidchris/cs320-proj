-App will persist data across reboots, needs to be able to write JSON to device disk.
-Synchronization will perform the Sync to Dynamo. (Other DB Drivers for JS can be utilized/make it easily replaceable)
-Create Inspection Forms
    -Initial creation of Establishment.
    -Think form elements for the second page of the app (violations/documentation/notes)
    -Submission page, allows for signing and printing.
-Food Code needs to be parsed to JSON for storage on device.


Relevancy of libraries
-Knockout, integrating view with model.
-Persistencejs, to store data to on-device DB.
-AWS-SDK, dynamo.